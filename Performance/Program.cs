﻿using EF6x;
using EFCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics;

namespace Performance
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = "N";
            CreateNewDatabase();
            do
            {
                RunActionOnEFCore();
                Console.WriteLine("Do you want to continue. Y/N");
                input = Console.ReadLine();

            } while (input.ToLower() == "y");

        }


        private static void CreateNewDatabase()
        {
            Console.WriteLine("Do you want to create new Database. Y/N");
            string input = Console.ReadLine();
            var repo = new BlogsRepo();
            if (input.ToLower() == "y")
            {
                Stopwatch efc = new Stopwatch();
                efc.Start();
                repo.Init(true);
                efc.Stop();
                Console.WriteLine($"Old Database Deleted and New Database with EF Core: {efc.ElapsedMilliseconds} ms");                
            }
            else
            {
                Stopwatch efc = new Stopwatch();
                efc.Start();
                repo.Init();
                efc.Stop();
                Console.WriteLine($"New Database created with EF Core: {efc.ElapsedMilliseconds} ms");
            }
        }

        private static void RunActionOnEFCore()
        {
            var repo = new BlogsRepo();
            Stopwatch efc = new Stopwatch();
            efc.Start();
            repo.CreateData(1);
            efc.Stop();
            Console.WriteLine($"1 Blogs with 2 Posts created: {efc.ElapsedMilliseconds} ms");

            efc = new Stopwatch();
            efc.Start();
            repo.CreateData(2000);
            efc.Stop();
            Console.WriteLine($"2000 Blogs with 4000 Posts created: {efc.ElapsedMilliseconds} ms");

            efc = new Stopwatch();
            efc.Start();
            repo.UpdateData(2000);
            efc.Stop();
            Console.WriteLine($"2000 Blogs with 4000 Posts updated: {efc.ElapsedMilliseconds} ms");
        }
    }
}
