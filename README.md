# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This project is to compare the performance of EFCore 2 vs EF 6.1.3

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Project contains two console applications. Performance EF Core is to run test with EF Core 2 and PerformanceEF6x is to run same test with EF 6.1.3 against central Database Server.

* Database Configuration

Project contain EF6x and EFCore Projects. In both projects it has a BlogRepo and BlogRepoEF6 class. These classes contain a property of connectionstring.
you can change connection string according to your requirements.

* Dependencies

All dependencies can found through Nuget Package or in .Net Core Project inside CSPRJ File 

* Database configuration

Just configur the Database Connection String according to your requirements. This solution is working with SQL Server Database. 

* How to run tests

Run Console Application and follow the instruction displaying on Console

* Deployment instructions

This is just a Proof of concept which just show the performance of EF Core vs EF6.1.3. It has nothing to deploy.

### Contribution guidelines ###

* Writing tests

feel free

* Code review

feel free

* Other guidelines

feel free

### Who do I talk to? ###

* Repo owner or admin

you must not talk to admin if you want to just download and use it for your purpose. but if you want to ask any thing or want to extend project, please contact administrator

* Other community or team contact