﻿using EF6x;
using System;
using System.Diagnostics;

namespace PerformanceEF6x
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string input = "N";
            CreateNewDatabase();
            do
            {
                RunActionOnEF6();
                Console.WriteLine("Do you want to continue. Y/N");
                input = Console.ReadLine();

            } while (input.ToLower() == "y");


        }

        private static void CreateNewDatabase()
        {
            Console.WriteLine("Do you want to create new Database. Y/N");
            string input = Console.ReadLine();
            var repo = new BlogRepoEf6();
            if (input.ToLower() == "y")
            {                
                Stopwatch efc = new Stopwatch();
                efc.Start();
                repo.Init(true);
                efc.Stop();
                Console.WriteLine($"New Database created with EF 6.1.3: {efc.ElapsedMilliseconds} ms");
            }
            else
            {
                Stopwatch efc = new Stopwatch();
                efc.Start();
                repo.Init();
                efc.Stop();
                Console.WriteLine($"New Database created with EF 6.1.3: {efc.ElapsedMilliseconds} ms");
            }
        }

        private static void RunActionOnEF6()
        {

            var repo = new BlogRepoEf6();
            Stopwatch efc = new Stopwatch();           

            efc = new Stopwatch();
            efc.Start();
            repo.CreateData(1);
            efc.Stop();
            Console.WriteLine($"1 Blogs with 2 Posts created: {efc.ElapsedMilliseconds} ms");

            efc = new Stopwatch();
            efc.Start();
            repo.CreateData(2000);
            efc.Stop();
            Console.WriteLine($"2000 Blogs with 4000 Posts created: {efc.ElapsedMilliseconds} ms");

            efc = new Stopwatch();
            efc.Start();
            repo.UpdateData(2000);
            efc.Stop();
            Console.WriteLine($"2000 Blogs with 4000 Posts updated: {efc.ElapsedMilliseconds} ms");
        }
    }
}
