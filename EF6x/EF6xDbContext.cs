﻿using DataEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF6x
{
    public class EF6xDbContext : DbContext
    {        
        public EF6xDbContext(string nameOrConnectionString)
      : base(nameOrConnectionString)
        {
        }
               
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
    }
}
