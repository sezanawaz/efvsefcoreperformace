﻿using DataEntities;
using System;
using System.Linq;
using System.Data.Entity;
namespace EF6x
{
    public class BlogRepoEf6
    {
        public static string ConnectionStringEfPerf { get; } = "data source=OLESRV711;initial catalog=OLESRV741_EF6Perf;integrated security=SSPI;MultipleActiveResultSets=True;Pooling=false;";
        public void Init(bool deleteDb = false)
        {
            using (var ctx = new EF6xDbContext(ConnectionStringEfPerf))
            {
                if (deleteDb)
                {
                    if (ctx.Database.Exists())
                    {
                        // set the database to SINGLE_USER so it can be dropped
                        ctx.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "ALTER DATABASE [" + ctx.Database.Connection.Database + "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE");

                        // drop the database
                        ctx.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "USE master DROP DATABASE [" + ctx.Database.Connection.Database + "]");
                    }
                    
                }
                ctx.Database.CreateIfNotExists();
            }
        }
        public void CreateData(int count)
        {

            using (var ctx = new EF6xDbContext(ConnectionStringEfPerf))
            {
                for (int i = 0; i < count; i++)
                {
                    ctx.Blogs.Add(Helper.GetBlogs());
                }

                ctx.SaveChanges();
            }

        }

        public void UpdateData(int count)
        {

            using (var ctx = new EF6xDbContext(ConnectionStringEfPerf))
            {
                var blogs = ctx.Blogs.Include(x => x.Posts).Take(count).ToList();

                blogs.ForEach(x =>
                {
                    x.Modified = DateTime.Now;
                    x.Posts.ForEach(p => p.Modified = DateTime.Now);
                });

                ctx.SaveChanges();
                ctx.Dispose();
            }
        }
    }
}
