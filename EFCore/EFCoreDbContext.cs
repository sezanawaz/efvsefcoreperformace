﻿using DataEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EFCore
{
    public class EFCoreDbContext : DbContext
    {
        public EFCoreDbContext(DbContextOptions options)
      : base(options)
        {
            
        }
      
        public static string ConnectionStringEfCore { get; } = "data source=OLESRV711;initial catalog=OLESRV741_EfCorePerf;integrated security=SSPI;MultipleActiveResultSets=True;ConnectRetryCount=0;Pooling=false;";
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

       
    }
}