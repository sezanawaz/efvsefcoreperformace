﻿using DataEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EFCore
{
    public class BlogsRepo
    {
        private DbContextOptionsBuilder optionBuilder = new DbContextOptionsBuilder()
               .UseSqlServer(EFCoreDbContext.ConnectionStringEfCore);

        public void Init(bool deleteDb=false)
        {
            using (var ctx = new EFCoreDbContext(optionBuilder.Options))
            {
                if (deleteDb)
                {                    
                    ctx.Database.EnsureDeleted();                    
                }
                ctx.Database.EnsureCreated();
            }
        }
        public void CreateData(int count)
        {

            using (var ctx = new EFCoreDbContext(optionBuilder.Options))
            {

                for (int i = 0; i < count; i++)
                {
                    ctx.Add(Helper.GetBlogs());
                }
                ctx.SaveChanges();
            }
        }

        public void UpdateData(int count)
        {

            using (var ctx = new EFCoreDbContext(optionBuilder.Options))
            {
                var blogs = ctx.Blogs.Include(x => x.Posts).Take(count).ToList();

                blogs.ForEach(x =>
                {
                    x.Modified = DateTime.Now;
                    x.Posts.ForEach(p => p.Modified = DateTime.Now);
                });

                ctx.SaveChanges();
                ctx.Dispose();
            }
        }
    }
}
