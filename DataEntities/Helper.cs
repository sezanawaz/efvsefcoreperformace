﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntities
{
    public static class Helper
    {
        public static Blog GetBlogs()
        {
            var blog = new Blog()
            {
                BlogId = Guid.NewGuid(),
                Rating = 5,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Url = "https://blogs.msdn.microsoft.com/dotnet/2017/05/12/announcing-ef-core-2-0-preview-1/"
            };

            blog.Posts = CreateNewPosts();

            return blog;
        }

        private static List<Post> CreateNewPosts()
        {
            List<Post> posts = new List<Post>();
            posts.Add(
               new Post()
               {
                   Content = "This week we made Entity Framework Core 2.0 Preview 1 available. Entity Framework Core (EF Core) is a lightweight, extensible, and cross-platform version of Entity Framework. EF Core follows the same release cycle as .NET Core but can be used in multiple .NET platforms, including .NET Core 2.0 and .NET Framework 4.6 or newer.",
                   Title = "Announcing EF Core 2.0 Preview 1",
                   PostId = Guid.NewGuid(),
                   Created = DateTime.Now,
                   Modified = DateTime.Now

               });

            posts.Add(
                new Post()
                {
                    Content = "Applications based on ASP.NET Core 2.0 Preview 1 can already use EF Core 2.0 Preview 1. Also, existing ASP.NET Core applications can upgrade EF Core by upgrading to the 2.0 Preview 1 version of the ASP.NET Core meta-package and removing any references to older EF Core runtime packages. Other applications can upgrade by installing a 2.0 Preview 1 - compatible version of the EF Core provider.E.g.to install the SQL Server provider in Visual Studio:",
                    Title = "Installing or upgrading to 2.0 Preview 1",
                    PostId = Guid.NewGuid(),
                    Created = DateTime.Now,
                    Modified = DateTime.Now
                });

            return posts;
        }
    }
}
