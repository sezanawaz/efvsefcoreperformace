﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataEntities
{
    public class Blog
    {
        public Guid BlogId { get; set; }
        public string Url { get; set; }
        public int Rating { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public List<Post> Posts { get; set; }
    }
}
